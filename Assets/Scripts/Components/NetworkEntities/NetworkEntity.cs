using UnityEngine;
using Unity.Netcode;
using Unity.Netcode.Samples;
using Unity.Netcode.Components;

namespace Monomyto.diepio
{
    [RequireComponent(typeof(NetworkObject))]
    [RequireComponent(typeof(NetworkTransform))]
    public class NetworkEntity<Data> : NetworkBehaviour
    {
        public NetworkTransform NetworkTransform 
        {
            get 
            {
                if (_netWorkTransform == null) 
                {
                    _netWorkTransform = GetComponent<NetworkTransform>();
                }

                return _netWorkTransform;
            }
        }

        public Data DataReference => _data;
        public bool IsInitialized => _data != null;

        protected NetworkTransform _netWorkTransform;
        [SerializeField]
        private Data _data;

        public virtual void Initialize(Data data)
        {
            if (NetworkObject == null) 
            {
                Debug.LogError("NetObject is null !");
                return;
            }

            if (NetworkTransform == null) 
            {
                Debug.LogError("NetworkTransform is null !");
                return;
            }

            _data = data;
        }
    }
}