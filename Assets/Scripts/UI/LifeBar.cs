using UnityEngine;
using UnityEngine.UI;
using Unity.Netcode;
using System;
using System.Collections;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;

namespace Monomyto.diepio
{
    public class LifeBar : Entity<Character>
    {
        [SerializeField]
        private CanvasGroup _canvasGroup;
        [SerializeField]
        private Image _bar;
        [SerializeField, Range(0, 1)]
        private float _desiredValue;
        [SerializeField]
        private float _lerpSpeed = 0.125f;

        private Coroutine _displayingLifeBarRoutine;

        public override void Initialize(Character data)
        {
            base.Initialize(data);
            _desiredValue = data.DataReference.Stats.Life;
            NetworkManager.Singleton.OnClientDisconnectCallback += OnClientDisconnected;
        }

        private void Start()
        {
            _canvasGroup.alpha = NetworkManager.Singleton.LocalClientId == DataReference.DataReference.ClientId ? 0 : 1;
        }
        
        private void OnDisable()
        {
            if (NetworkManager.Singleton == null) 
            {
                return;
            }

            NetworkManager.Singleton.OnClientDisconnectCallback -= OnClientDisconnected;
        }

        private void OnClientDisconnected(ulong id)
        {
            if (id == DataReference.DataReference.ClientId) 
            {
                Destroy(gameObject);
            }
        }

        private void Update()
        {

            if (DataReference == null)
            {
                Destroy(gameObject);
                return;
            }

            if (!Mathf.Approximately(_desiredValue, DataReference.DataReference.Stats.Life) && _displayingLifeBarRoutine == null && NetworkManager.Singleton.LocalClientId == DataReference.DataReference.ClientId) 
            {
                _displayingLifeBarRoutine = StartCoroutine(DisplayLifeBar());                
            }

            _desiredValue = Mathf.Lerp(_desiredValue, DataReference.DataReference.Stats.Life, .125f);
            _bar.fillAmount = _desiredValue;

            transform.position = (Vector2)DataReference.transform.position + (Vector2.up * -1f);
        }

        private IEnumerator DisplayLifeBar() 
        {
            Debug.Log("Display bar !");
            _canvasGroup.alpha = 0;
            yield return new WaitForEndOfFrame();

            TweenerCore<float, float, FloatOptions> fade = _canvasGroup.DOFade(1, 1.5f);
            yield return fade.WaitForCompletion();

            while (!Mathf.Approximately(_desiredValue, DataReference.DataReference.Stats.Life)) 
            {
                yield return null;
            }

            yield return new WaitForSeconds(3f);
            TweenerCore<float, float, FloatOptions> fadeOut = _canvasGroup.DOFade(0, 1.5f);
            yield return fadeOut.WaitForCompletion();
            _displayingLifeBarRoutine = null;
        }
    }
}