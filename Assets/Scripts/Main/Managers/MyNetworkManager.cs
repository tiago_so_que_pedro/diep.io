using UnityEngine;
using Unity.Netcode;

namespace Monomyto.diepio
{
    public struct ServerLog
    {
        public const string ServerColor = "#F6820D";

        public string Message;
        public string Color;

        public ServerLog(string message, string color)
        {
            Message = message;
            Color = color;
        }
    }

    public class MyNetworkManager : ManagerObject
    {

        #region - Client side - 

        public void Join(string name)
        {
            NetworkManager.Singleton.NetworkConfig.ConnectionData = System.Text.Encoding.ASCII.GetBytes(name);
            if (!NetworkManager.Singleton.IsClient)
            {
                NetworkManager.Singleton.StartClient();
            }
            else 
            {
                CreatePlayerServerRpc(NetworkManager.Singleton.LocalClientId, name);
            }
        }

        [ClientRpc]
        public void ResponseSendCharacterDataToClientRpc(ulong networkObjectId, string characterDataString) 
        {
            CharacterData data = Newtonsoft.Json.JsonConvert.DeserializeObject<CharacterData>(characterDataString);
            
            if (!Game.GetManager<CharacterManager>().InGameCharacterByObjectId.ContainsKey(networkObjectId)) 
            {
                return;
            }

            Game.GetManager<CharacterManager>().InGameCharacterByObjectId[networkObjectId].Initialize(data);
            if (data.ClientId == NetworkManager.Singleton.LocalClientId) 
            {
                Game.GetManager<CharacterManager>().LocalNetworkObjectId = networkObjectId;
            }
        }


        [ClientRpc]
        public void NotifyAboutDisabledPlayerClientRpc(ulong clientObjectId) 
        {
            //Actions that will be performed on all other clients when clientObjectId is disconnected or removed
    
            //1. Remove client from InGameCharacterByObjecId list
            Game.GetManager<CharacterManager>().InGameCharacterByObjectId.Remove(clientObjectId);
        }

        [ClientRpc]
        public void SendWeaponDataToClientRpc(ulong objectId, string weaponDataString)
        {
            WeaponData weaponData = Newtonsoft.Json.JsonConvert.DeserializeObject<WeaponData>(weaponDataString);

            if (!Game.GetManager<CharacterManager>().InGameCharacterByObjectId.ContainsKey(objectId)) 
            {
                return;
            }

            Game.GetManager<CharacterManager>().InGameCharacterByObjectId[objectId].WeaponComponent.Initialize(weaponData);
        }

        [ClientRpc]
        public void SendScoreToClientRpc(int score, ulong client) 
        {
            if (NetworkManager.Singleton.LocalClientId == client) 
            {
                Game.GetManager<MatchManager>().Score += score;
            }
        }

        #endregion

        #region - Server side - 

        protected override void OnObjectDisabled()
        {
            Game.UnregisterManager(this);
        }

        protected override void OnObjectEnabled()
        {
            Game.RegisterManager(this);
        }

        private void Start()
        {
#if SERVER
            NetworkManager.Singleton.OnServerStarted += OnServerStarted;
            NetworkManager.Singleton.StartServer();
#elif UNITY_EDITOR
            if (PlayerPrefs.GetInt((ParrelSync.ClonesManager.IsClone() ? "Clone_" : "Original_") + "EditorPlayAsServer", 0) == 1)
            {
                NetworkManager.Singleton.OnServerStarted += OnServerStarted;
                NetworkManager.Singleton.StartServer();
            }
#endif
        }

        public override bool Run()
        {
#if SERVER || UNITY_EDITOR
            if (IsServer && Game.CurrentState != Game.GameState.Server)
            {
                Game.SetState(Game.GameState.Server);
            }
#endif
            if (base.Run())
            {
                return true;
            }

            return false;

        }

        private void OnServerStarted()
        {
            if (NetworkManager.Singleton.IsServer)
            {
                NetworkManager.Singleton.ConnectionApprovalCallback += OnConnectionApprovalCallback;
                NetworkManager.Singleton.OnClientDisconnectCallback += OnClientDisconnect;


                Game.GetManager<BoxManager>().PopulateBoxes();
                Game.GetManager<CharacterManager>().SpawnRandomEnemies();
            }
        }

        private void OnConnectionApprovalCallback(byte[] connectionData, ulong clientId, NetworkManager.ConnectionApprovedDelegate callback)
        {
            string name = System.Text.Encoding.ASCII.GetString(connectionData);

            callback(false, null, true, Vector3.zero, Quaternion.identity);

            if (NetworkManager.Singleton.ConnectedClients.TryGetValue(clientId, out NetworkClient client))
            {
                Game.GetManager<CharacterManager>().ServerCreatePlayer(new CharacterData(CharacterStats.Default, clientId, name), client);
                ServerLog(new ServerLog($"{ name } has joined.", diepio.ServerLog.ServerColor));
            }
        }

        private void OnClientDisconnect(ulong clientId)
        {
            if (NetworkManager.Singleton.ConnectedClients.TryGetValue(clientId, out NetworkClient client))
            {
                ServerLog(new ServerLog($"{ client.ClientId } has disconnected", diepio.ServerLog.ServerColor));
                Game.GetManager<CharacterManager>().ServerRemovePlayer(client);
            }
        }

        [ServerRpc(RequireOwnership = false)]
        public void CreatePlayerServerRpc(ulong clientId, string displayName) 
        {
            NetworkClient client = NetworkManager.Singleton.ConnectedClients[clientId];
            Game.GetManager<CharacterManager>().ServerCreatePlayer(new CharacterData(CharacterStats.Default, clientId, displayName), client);
        }

        [ServerRpc(RequireOwnership = false)]
        public void CharacterFaceRequestServerRpc(ulong clientId, Vector3 direction)
        {
            if (NetworkManager.Singleton.ConnectedClients.TryGetValue(clientId, out NetworkClient client))
            {
                Game.GetManager<CharacterManager>().ServerRotatePlayer(client, direction);
            }
        }

        [ServerRpc(RequireOwnership = false)]
        public void CharacterMoveRequestServerRpc(ulong clientId, Vector2 direction)
        {
            if (NetworkManager.Singleton.ConnectedClients.TryGetValue(clientId, out NetworkClient client))
            {
                Game.GetManager<CharacterManager>().ServerMovePlayer(client, direction);
            }
        }

        [ServerRpc(RequireOwnership = false)]
        public void CharacterStopMoveServerRpc(ulong clientId, Vector2 direction)
        {
            if (NetworkManager.Singleton.ConnectedClients.TryGetValue(clientId, out NetworkClient client))
            {
                Game.GetManager<CharacterManager>().ServerStopMovePlayer(client, direction);
            }
        }

        [ServerRpc(RequireOwnership = false)]
        public void RequestCharacterDataServerRpc(ulong clientId) 
        {
            NetworkClient client = NetworkManager.Singleton.ConnectedClients[clientId];
            CharacterData desiredData = Game.GetManager<CharacterManager>().InGamePlayers[client].DataReference;
            string desiredDataString = Newtonsoft.Json.JsonConvert.SerializeObject(desiredData);

            ResponseSendCharacterDataToClientRpc(Game.GetManager<CharacterManager>().InGamePlayers[client].NetworkObjectId, desiredDataString);
        }

        [ServerRpc(RequireOwnership = false)]
        public void RequestForShootServerRpc(ulong clientId) 
        {
            ServerLog(new diepio.ServerLog($"{ clientId } requested for shoot", diepio.ServerLog.ServerColor));
            if (NetworkManager.Singleton.ConnectedClients.TryGetValue(clientId, out NetworkClient client)) 
            {
                Game.GetManager<WeaponManager>().Shoot(client);
            }
        }

        #endregion

        #region - Server log - 

        public static void ServerLog(ServerLog log) 
        {
            UIPanel_Server.ServerLog.Add(log);
            UIPanel_Server.OnLog?.Invoke();
        }

        public static void ClearLog() 
        {
            UIPanel_Server.ServerLog.Clear();
            UIPanel_Server.OnLog?.Invoke();
        }

        #endregion

        private void OnApplicationQuit()
        {
#if UNITY_EDITOR
            PlayerPrefs.SetInt((ParrelSync.ClonesManager.IsClone() ? "Clone_" : "Original_" ) +  "EditorPlayAsServer", 0);
#endif
        }
    }
}