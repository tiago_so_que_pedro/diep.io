using UnityEngine;
using Unity.Netcode;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;

namespace Monomyto.diepio
{
    public class BulletComponent : NetworkEntity<BulletData>, IMonoBehaviourPool
    {
        public NetworkVariable<ulong> ClientId;
        public NetworkVariable<ulong> ShooterObjectId;

        [SerializeField]
        private SpriteRenderer _sprite;

        private float _travelDuration;

        public MonoBehaviour MonoBehaviourReference => this;

        public GameObject GameObjectReference => gameObject;
        public NetworkObject NetworkObjectReference => NetworkObject;

        TweenerCore<Vector3, Vector3, VectorOptions> _destroyingAnimation;

        bool _canMove;

        public override void Initialize(BulletData data)
        {
            base.Initialize(data);

            _canMove = true;

            transform.position = data.StartPosition;
        }

        public override void OnNetworkSpawn()
        {
            if (!NetworkManager.Singleton.IsServer)
            {
                ShooterObjectId.OnValueChanged += OnShooterChanged;
            }
            
            _sprite.transform.localScale = Vector3.one;
            _sprite.color = new Color(_sprite.color.r, _sprite.color.g, _sprite.color.b, 1);
            _destroyingAnimation = null;
        }

        public override void OnNetworkDespawn()
        {
            if (!NetworkManager.Singleton.IsServer) 
            {
                ShooterObjectId.OnValueChanged -= OnShooterChanged; 
            }
        }

        private void Update()
        {
            if (NetworkManager.Singleton.IsServer)
            {
                _travelDuration += Time.deltaTime;
                
                if (_canMove)
                {
                    transform.position = Vector3.Lerp(DataReference.StartPosition, DataReference.Destination, _travelDuration / DataReference.TravelDuration);
                }

                if (_travelDuration >= DataReference.TravelDuration + 5)
                {
                    if (_destroyingAnimation == null)
                    {
                        //Kill Bullet instance - Moving back to pool
                        _destroyingAnimation = _sprite.transform.DOScale(Vector2.one * 2.5f, 1f).OnComplete(() =>
                        {
                            NetworkObject.Despawn();
                            _travelDuration = 0;
                            _destroyingAnimation = null;
                        });
                    }
                }
                else 
                {
                    DetectDamage();
                }
            }


            //Lerp sprite alpha by its scale
            float lerpValue = _sprite.transform.localScale.x / 2.5f;
            Color toColor = new Color(_sprite.color.r, _sprite.color.g, _sprite.color.b, Mathf.Lerp(1, 0, lerpValue));
            _sprite.color = toColor;
        }

        private void DetectDamage() 
        {
            Collider2D detectedEnemy = Physics2D.OverlapCircle(transform.position, .15f);

            if (DataReference.Shooter == null || detectedEnemy == null) { return; }
            
            if (Game.GetManager<CharacterManager>().InGameCharacterColliders.TryGetValue(detectedEnemy, out Character character)) 
            {
                if (character.Team.Value != DataReference.Shooter.Team.Value) 
                {
                    MyNetworkManager.ServerLog(new ServerLog($"{ DataReference.Shooter.DataReference.DisplayName } deal damage on { character.DataReference.DisplayName }", ServerLog.ServerColor));

                    //Deal damage and send to clients
                    character.DataReference.Stats.Life -= DataReference.Shooter.WeaponComponent.WeaponSO.Damage;

                    if(character is Player) 
                    {
                        Game.GetManager<MyNetworkManager>().ResponseSendCharacterDataToClientRpc(character.NetworkObjectId, Newtonsoft.Json.JsonConvert.SerializeObject(character.DataReference));
                    }
                    
                    _canMove = false;
                    _travelDuration = DataReference.TravelDuration + 5;

                    if (character.DataReference.Stats.Life <= 0)
                    {
                        if (character is Player)
                        {
                            Game.GetManager<CharacterManager>().ServerKillPlayer(character.DataReference.ClientId);
                        }
                        else if (character is Enemy)
                        {
                            Game.GetManager<CharacterManager>().ServerRemoveCollider(character);
                            character.NetworkObject.Despawn();

                            if (DataReference.Shooter is Player) 
                            {
                                Game.GetManager<MyNetworkManager>().SendScoreToClientRpc(10, DataReference.Shooter.DataReference.ClientId);
                            }
                        }
                    }
                }
            }
        }

        private void OnShooterChanged(ulong previousValue, ulong newValue)
        {
            Character character = Game.GetManager<CharacterManager>().InGameCharacterByObjectId[newValue];
            _sprite.color = character.Sprite.color;
        }

        void OnDrawGizmos() 
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, .15f);
        }
    }
}