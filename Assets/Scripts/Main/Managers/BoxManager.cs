using Unity.Netcode;
using UnityEngine;

namespace Monomyto.diepio
{
    public class BoxManager : ManagerObject
    {

        [SerializeField]
        private NetworkObject _boxPrefab;

        protected override void OnObjectDisabled()
        {
            Game.UnregisterManager(this);
        }

        protected override void OnObjectEnabled()
        {
            Game.RegisterManager(this);
        }

        public void PopulateBoxes() 
        {
            for (int i = 0; i < 20; i++) 
            {
                CreateBox();
            }
        }

        public void CreateBox() 
        {
            PoolManager poolManager = Game.GetManager<PoolManager>();
            Vector3 position = new Vector3(Random.Range(-26f, 26), Random.Range(-19f, 19f), 0);
            NetworkObject networkBox = poolManager.GetInstanceFromPool(_boxPrefab, position, Quaternion.identity);

            if (!poolManager.BoxPerNetworkObject.TryGetValue(networkBox, out Box box))
            {
                box = networkBox.GetComponent<Box>();
                poolManager.BoxPerNetworkObject.Add(networkBox, box);
            }

            box.transform.localScale = Vector3.one;
            box.Initialize(Random.Range(0.0f, 1.0f) < .5f ? 0 : 1);
            networkBox.SpawnWithOwnership(OwnerClientId);
        }
    }
}