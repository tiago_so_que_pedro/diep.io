using UnityEngine;

namespace Monomyto.diepio 
{
    [System.Serializable]
    public class WeaponData 
    {

        public static WeaponData Default => new WeaponData("weapon_default");

        public string WeaponId => _weaponId;
        public float FireDelay 
        {
            get => _fireDelay;
            set => _fireDelay = value;
        }

        [SerializeField]
        private string _weaponId;
        [SerializeField]
        private float _fireDelay;

        public WeaponData(string weaponId) 
        {
            _weaponId = weaponId;
            _fireDelay = 0;
        }
    }
}
