using UnityEngine;
using System.Linq;
using Unity.Netcode;

namespace Monomyto.diepio
{
    public class Enemy : Character
    {

        public enum EnemyState { Awarness, Chase }

        public EnemyState CurrentState => _currentState;

        public Character DetectedEnemy => _detectedEnemy;

        [SerializeReference]
        private EnemyState _currentState;
        [SerializeField]
        private float _detectRange;
        [SerializeField]
        private float _shootDelay;

        private Character _detectedEnemy;

        protected override void Update()
        {

            SetSpriteColor();
            AI();
        }

        private void SetSpriteColor() 
        {
            if (!NetworkManager.Singleton.IsServer)
            {
                CharacterManager characterManager = Game.GetManager<CharacterManager>();
                Character localPlayer = characterManager.InGameCharacterByObjectId.Where(x => x.Value.NetworkObjectId == characterManager.LocalNetworkObjectId).FirstOrDefault().Value;

                if (localPlayer == null)
                {
                    return;
                }

                Sprite.color = localPlayer.Team.Value == Team.Value ? Color.blue : Color.red;
            }
        }

        private void AI() 
        {
            if (NetworkManager.Singleton.IsServer)
            {
                switch (_currentState)
                {
                    case EnemyState.Awarness:
                        if (IsCharacterDetected())
                        {
                            _currentState = EnemyState.Chase;
                            _shootDelay = Time.time + 2f;

                        }
                        break;
                    case EnemyState.Chase:
                        {
                            if (!IsCharacterDetected())
                            {
                                _currentState = EnemyState.Awarness;
                            }
                            else
                            {
                                //Chase
                                Vector2 direction = (_detectedEnemy.transform.position - transform.position).normalized;

                                base.Face(direction);
                                base.Move(direction);

                                if (Time.time > _shootDelay) 
                                {
                                    _shootDelay = Time.time + 2f;
                                    Game.GetManager<WeaponManager>().Shoot(this);
                                }
                            }
                        }
                        break;
                }
            }
        }

        private bool IsCharacterDetected()
        {

            Collider2D[] detectedTargets = Physics2D.OverlapCircleAll(transform.position, _detectRange);

            if (detectedTargets.Length == 0)
            {

                _detectedEnemy = null;
                return false;
            }

            if (_detectedEnemy != null) 
            {
                return true;
            }

            //Check if one of the detected colliders are from his enemies, by validating his team
            foreach (Collider2D collider in detectedTargets)
            {
                if (!Game.GetManager<CharacterManager>().InGameCharacterColliders.TryGetValue(collider, out Character character))
                {
                    continue;
                }

                if (character.Team.Value != Team.Value) 
                {
                    _detectedEnemy = character;
                    break;
                }
            }

            return _detectedEnemy != null;
        }

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, _detectRange);
        }
#endif
    }
}
