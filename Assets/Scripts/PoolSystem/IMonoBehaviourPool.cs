using UnityEngine;
using Unity.Netcode;

public interface IMonoBehaviourPool
{
    MonoBehaviour MonoBehaviourReference { get; }
    GameObject GameObjectReference { get; }

    NetworkObject NetworkObjectReference { get; }
}