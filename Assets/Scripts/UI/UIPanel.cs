using UnityEngine;

namespace Monomyto.diepio
{
    public abstract class UIPanel : MonoBehaviour
    {

        public Game.GameState TargetState => _targetState;

        public bool WaitLocalClientConnection => _waitClientLocalClientConnection;

        [SerializeField]
        private Game.GameState _targetState;

        [SerializeField]
        private bool _waitClientLocalClientConnection;

        private void OnEnable()
        {
            Open();
        }

        public abstract void Open();
    }
}