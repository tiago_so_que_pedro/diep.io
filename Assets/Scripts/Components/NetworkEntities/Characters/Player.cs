using UnityEngine;
using Unity.Netcode;
using System.Linq;

namespace Monomyto.diepio
{
    public class Player : Character
    {

        private Vector3 _lastMousePosition;

        protected override void Update()
        {
            base.Update();

            SetPlayerColor();

            Move(Vector2.zero);
            Face(Vector3.zero);
        }

        protected override void Start()
        {
            base.Start();

            if (!NetworkManager.Singleton.IsServer)
            {
                //Request for its CharacterData and then initialize it
                Game.GetManager<MyNetworkManager>().RequestCharacterDataServerRpc(NetworkManager.Singleton.LocalClientId);
            }
        }

        public override void Move(Vector2 direction)
        {
            if (IsServer)
            {
                base.Move(WalkX.Value + WalkY.Value);
                return;
            }

            if (Input.GetKey(KeyCode.W) && WalkY.Value == Vector2.zero)
            {
                Game.GetManager<MyNetworkManager>().CharacterMoveRequestServerRpc(NetworkManager.Singleton.LocalClientId, Vector2.up);
            }
            else if (Input.GetKey(KeyCode.S) && WalkY.Value == Vector2.zero)
            {
                Game.GetManager<MyNetworkManager>().CharacterMoveRequestServerRpc(NetworkManager.Singleton.LocalClientId, -Vector2.up);
            }
            
            if (Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.S))
            {
                Game.GetManager<MyNetworkManager>().CharacterStopMoveServerRpc(NetworkManager.Singleton.LocalClientId, Vector2.up);
            }

            if (Input.GetKey(KeyCode.A) && WalkX.Value == Vector2.zero)
            {
                Game.GetManager<MyNetworkManager>().CharacterMoveRequestServerRpc(NetworkManager.Singleton.LocalClientId, -Vector2.right);
            }
            else if (Input.GetKey(KeyCode.D) && WalkX.Value == Vector2.zero)
            {
                Game.GetManager<MyNetworkManager>().CharacterMoveRequestServerRpc(NetworkManager.Singleton.LocalClientId, Vector2.right);
            }
            
            if (Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D)) 
            {
                Game.GetManager<MyNetworkManager>().CharacterStopMoveServerRpc(NetworkManager.Singleton.LocalClientId, Vector2.right);
            }
        }

        public override void Face(Vector3 direction)
        {
            if (!IsServer && _lastMousePosition != Input.mousePosition)
            {
                _lastMousePosition = Input.mousePosition;
                _lastMousePosition = Game.GameCamera.ScreenToWorldPoint(_lastMousePosition);
                Game.GetManager<MyNetworkManager>().CharacterFaceRequestServerRpc(NetworkManager.Singleton.LocalClientId, _lastMousePosition);
            }
            else if(IsServer && direction != Vector3.zero && direction != _lastMousePosition)
            {
                _lastMousePosition = (Vector2)(direction - transform.position).normalized;
                base.Face(_lastMousePosition);
            }

        }

        private void SetPlayerColor() 
        {
            if (NetworkManager.Singleton.LocalClientId == DataReference.ClientId)
            {
                _sprite.color = Color.blue;
                gameObject.layer = (int)Mathf.Log(Game.GetManager<LayerManager>().FriendLayer);
            }
            else
            {
                //Local charcter - AKA. You
                if (_localCharacterReference == null)
                {
                    _localCharacterReference = Game.GetManager<CharacterManager>().InGameCharacterByObjectId.Where(x => x.Value.DataReference.ClientId == NetworkManager.Singleton.LocalClientId).FirstOrDefault().Value;
                }

                if (_localCharacterReference == null)
                {

                    _sprite.color = Color.red;
                    return;
                }

                if (_localCharacterReference.Team.Value != Team.Value)
                {
                    _sprite.color = Color.red;
                    gameObject.layer = (int)Mathf.Log(Game.GetManager<LayerManager>().EnemyLayer);
                }
                else
                {
                    _sprite.color = Color.blue;
                    gameObject.layer = (int)Mathf.Log(Game.GetManager<LayerManager>().FriendLayer);
                }
            }
        }
    }
}