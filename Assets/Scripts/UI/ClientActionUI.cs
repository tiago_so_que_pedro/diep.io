using UnityEngine;
using Unity.Netcode;
using TMPro;
using UnityEngine.UI;

namespace Monomyto.diepio
{
    public class ClientActionUI : Entity<NetworkClient>
    {

        [SerializeField]
        private TextMeshProUGUI _infoLabel;
        [SerializeField]
        private Button _button;

        public override void Initialize(NetworkClient data)
        {
            base.Initialize(data);

            if (!Game.GetManager<CharacterManager>().InGamePlayers.TryGetValue(data, out Character character)) 
            {
                return;
            }

            NetworkManager.Singleton.OnClientDisconnectCallback += OnClientDisconnect;
            

            _button.onClick.AddListener(()=>
            {
                DealDamage();   
            });
        }

        void DealDamage() 
        {
            if (!Game.GetManager<CharacterManager>().InGamePlayers.TryGetValue(DataReference, out Character character))
            {
                return;
            }

            character.DataReference.Stats.Life -= .02f;
            Game.GetManager<MyNetworkManager>().ResponseSendCharacterDataToClientRpc(character.NetworkObjectId, Newtonsoft.Json.JsonConvert.SerializeObject(character.DataReference));

            if (character.DataReference.Stats.Life <= 0)
            {
                Game.GetManager<CharacterManager>().ServerKillPlayer(character.DataReference.ClientId);
            }
        }

        private void Update()
        {
            if (!Game.GetManager<CharacterManager>().InGamePlayers.TryGetValue(DataReference, out Character character))
            {
                return;
            }

            string info = $"{ character.DataReference.DisplayName } Life: { character.DataReference.Stats.Life }";
            _infoLabel.text = info;
        }

        void OnClientDisconnect(ulong id) 
        {
            if (id == DataReference.ClientId)
            {
                NetworkManager.Singleton.OnClientDisconnectCallback -= OnClientDisconnect;
                Destroy(gameObject);
            }
        }
    }
}