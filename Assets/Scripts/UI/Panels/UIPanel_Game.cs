using UnityEngine;
using Unity.Netcode;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.UI;

namespace Monomyto.diepio
{
    public class UIPanel_Game : UIPanel
    {
        [SerializeField]
        private ClientDisplayNameUI _textLblPrefab;
        [SerializeField]
        private LifeBar _lifebarPrefab;

        private List<ulong> _charactersInitialized;

        [SerializeField]
        private Image _ammoBar;
        [SerializeField]
        private float _currentFillAmount;

        public override void Open()
        {
        }

        private void Start()
        {
            _charactersInitialized = new List<ulong>();

            OnClientConnected(NetworkManager.Singleton.LocalClientId);
            NetworkManager.Singleton.OnClientDisconnectCallback += OnClientDisconnected;
            _currentFillAmount = 1;
        }

        private void OnDisable()
        {
            if (NetworkManager.Singleton == null) { return; }
            NetworkManager.Singleton.OnClientDisconnectCallback -= OnClientDisconnected;
            _charactersInitialized.Clear();
        }

        private void Update()
        {
            if (_charactersInitialized.Count != Game.GetManager<CharacterManager>().InGameCharacterByObjectId.Count) 
            {
                foreach (Character character in Game.GetManager<CharacterManager>().InGameCharacterByObjectId.Values) 
                {
                    if (!_charactersInitialized.Contains(character.DataReference.ClientId)) 
                    {
                        OnClientConnected(character.DataReference.ClientId);
                    }
                }
            }

            if (!NetworkManager.Singleton.IsServer) 
            {
                CharacterManager characterManager = Game.GetManager<CharacterManager>();
                if (characterManager.InGameCharacterByObjectId.TryGetValue(characterManager.LocalNetworkObjectId, out Character character)) 
                {
                    float curAmmo = character.WeaponComponent.CurrentAmmo.Value;
                    float toValue = curAmmo / character.WeaponComponent.WeaponSO.MaxAmmo;
                    _currentFillAmount = Mathf.Lerp(_currentFillAmount, toValue, .125f);
                    _ammoBar.fillAmount = _currentFillAmount;
                }
            }
        }

        private void OnClientConnected(ulong clientId) 
        {
            Character character = Game.GetManager<CharacterManager>().InGameCharacterByObjectId.Where(x => x.Value.DataReference.ClientId == clientId).FirstOrDefault().Value;

            if (character == null) 
            {
                return;
            }

            ClientDisplayNameUI _clientName = Instantiate(_textLblPrefab, transform);
            LifeBar _clientLifeBar = Instantiate(_lifebarPrefab, transform);

            _clientName.Initialize(character);
            _clientLifeBar.Initialize(character);

            _charactersInitialized.Add(clientId);
        }

        private void OnClientDisconnected(ulong clientId) 
        {
            _charactersInitialized.Remove(clientId);
        }
    }
}