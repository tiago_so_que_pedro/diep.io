using UnityEngine;
using Unity.Netcode;

namespace Monomyto.diepio
{
    public class LocalCameraController : MonoBehaviour
    {
        private void LateUpdate()
        {
            if (NetworkManager.Singleton.IsServer || Game.CurrentState != Game.GameState.Gameplay) { return; }

            ulong localNetworkObjectId = Game.GetManager<CharacterManager>().LocalNetworkObjectId;
            if (Game.GetManager<CharacterManager>().InGameCharacterByObjectId.TryGetValue(localNetworkObjectId, out Character character)) 
            {
                if (character.DataReference.ClientId == NetworkManager.Singleton.LocalClientId) 
                {
                    Vector3 toPosition = new Vector3(character.transform.position.x, character.transform.position.y, -100);
                    transform.position = toPosition;
                }
            }
        }
    }
}