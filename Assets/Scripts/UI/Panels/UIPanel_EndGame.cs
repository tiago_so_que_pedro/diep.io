using UnityEngine;
using System.Collections;
using DG.Tweening;
using TMPro;

namespace Monomyto.diepio
{
    public class UIPanel_EndGame : UIPanel
    {
        [SerializeField]
        private CanvasGroup _matchInfoCanvasGroup;
        [SerializeField]
        private TextMeshProUGUI _score;
        
        public override void Open()
        {
            StartCoroutine(IEDisplayMatchInfo());
        }

        private void Update()
        {
            if (Input.anyKeyDown) 
            {
                Game.SetState(Game.GameState.Intro);
            }
        }

        IEnumerator IEDisplayMatchInfo() 
        {
            _matchInfoCanvasGroup.alpha = 0;
            yield return new WaitForEndOfFrame();
            
            _matchInfoCanvasGroup.DOFade(1, 2f);
            _score.text = "Score: \n" + Game.GetManager<MatchManager>().Score;
        }
    }
}