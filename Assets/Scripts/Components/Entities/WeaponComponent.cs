using UnityEngine;
using Unity.Netcode;

namespace Monomyto.diepio
{
    public class WeaponComponent : NetworkEntity<WeaponData>
    {

        public WeaponGraphics WeaponGraphics => _weaponGraphics;
        public WeaponSO WeaponSO => _currentWeaponSO;

        public NetworkVariable<int> CurrentAmmo = new NetworkVariable<int>();

        [SerializeField]
        private Character _owner;
        [SerializeField]
        private Transform _gunParent;

        [SerializeField]
        private WeaponSO _currentWeaponSO;
        private WeaponGraphics _weaponGraphics;

        public override void Initialize(WeaponData data)
        {
            base.Initialize(data);

            if (_weaponGraphics != null) 
            {
                Destroy(_weaponGraphics.gameObject);
            }

            _currentWeaponSO = Game.GetManager<WeaponManager>().GetWeaponSOById(data.WeaponId);
            _weaponGraphics = Instantiate(_currentWeaponSO.WeaponGraphics, _gunParent);
            _weaponGraphics.transform.localPosition = Vector3.zero;

            //Current ammo will be available only on server side
            if (NetworkManager.Singleton.IsServer) 
            {
                CurrentAmmo.Value = _currentWeaponSO.MaxAmmo;
            }
        }

        private void Update()
        {
            if (_owner == null && NetworkManager.Singleton.LocalClientId != _owner.DataReference.ClientId) 
            {
                return;
            }

            bool canShoot = CanShoot();

            if (Input.GetMouseButton(0) && canShoot) 
            {
                Game.GetManager<MyNetworkManager>().RequestForShootServerRpc(_owner.DataReference.ClientId);
            }
        }

        private bool CanShoot() 
        {
            bool canShoot = true;
            if (DataReference.FireDelay < 1)
            {
                DataReference.FireDelay += Time.deltaTime / _currentWeaponSO.FireRate;
                canShoot = false;
            }
            else 
            {
                DataReference.FireDelay = 0;
            }

            return canShoot;
        }
    }
}