using UnityEngine;
using UnityEditor;
using UnityToolbarExtender;

#if UNITY_EDITOR
[InitializeOnLoad()]
public static class ClientPlayButtonEditor
{
    static ClientPlayButtonEditor() 
    {
        ToolbarExtender.LeftToolbarGUI.Clear();
        ToolbarExtender.RightToolbarGUI.Add(PlayAsServer);
    }

    static void PlayAsServer() 
    {
        if (GUILayout.Button("Server")) 
        {
            PlayerPrefs.SetInt((ParrelSync.ClonesManager.IsClone() ? "Clone_" : "Original_") + "EditorPlayAsServer", 1);
            EditorApplication.isPlaying = true;
        }
    }
}
#endif
