using UnityEngine;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using Unity.Netcode;

namespace Monomyto.diepio
{
    public class Box : NetworkEntity<int>
    {

        [SerializeField]
        private SpriteRenderer _sprite;

        public NetworkVariable<int> Team;

        TweenerCore<Vector3, Vector3, VectorOptions> _destroyingAnimation;

        private void Update()
        {
            if (!NetworkManager.Singleton.IsServer)
            {
                CharacterManager characterManager = Game.GetManager<CharacterManager>();
                if (!characterManager.InGameCharacterByObjectId.TryGetValue(characterManager.LocalNetworkObjectId, out Character character))
                {
                    return;
                }

                _sprite.color = IsSameTeam(character) ? character.Sprite.color : Color.red;
            }
            else 
            {
                DetectDamage();
            }

            //Lerp sprite alpha by its scale
            float lerpValue = _sprite.transform.localScale.x / 0.3f;
            Color toColor = new Color(_sprite.color.r, _sprite.color.g, _sprite.color.b, Mathf.Lerp(1, 0, lerpValue));
            _sprite.color = toColor;
        }

        public override void Initialize(int data)
        {
            base.Initialize(data);

            if(NetworkManager.Singleton.IsServer) 
            {
                Team.Value = data;
            }
        }

        public override void OnNetworkSpawn()
        {
            base.OnNetworkSpawn();
            
            float direction = Random.Range(0.0f, 1.0f) < .5f ? -1 : 1;
            _sprite.transform.DORotate((Vector3.forward * direction) * 540, 5f).SetLoops(-1);

            _sprite.transform.localScale = Vector3.one * 0.09f;
            _sprite.color = new Color(_sprite.color.r, _sprite.color.g, _sprite.color.b, 1);
            _destroyingAnimation = null;
        }

        public override void OnNetworkDespawn()
        {
            if (NetworkManager.Singleton.IsServer) 
            {
                Game.GetManager<BoxManager>().CreateBox();
            }
        }

        public bool IsSameTeam(Character character) 
        {
            return character != null && character.Team.Value == Team.Value;
        }

        private void DetectDamage()
        {
            Collider2D detectedEnemy = Physics2D.OverlapCircle(transform.position, .15f);
            if (detectedEnemy == null) { return; }
            if (Game.GetManager<CharacterManager>().InGameCharacterColliders.TryGetValue(detectedEnemy, out Character character) && _destroyingAnimation == null)
            {   
                MyNetworkManager.ServerLog(new ServerLog($"{ character.DataReference.DisplayName } found a box", ServerLog.ServerColor));

                //Deal damage or heal and then send to clients
                bool isSameTeam = IsSameTeam(character);
                character.DataReference.Stats.Life += isSameTeam ?  .15f : -.15f;
                character.DataReference.Stats.Life = Mathf.Clamp01(character.DataReference.Stats.Life);
                Game.GetManager<MyNetworkManager>().ResponseSendCharacterDataToClientRpc(character.NetworkObjectId, Newtonsoft.Json.JsonConvert.SerializeObject(character.DataReference));

                if (character is Player && character.DataReference.Stats.Life <= 0)
                {
                    Game.GetManager<CharacterManager>().ServerKillPlayer(character.DataReference.ClientId);
                }

                if (isSameTeam) 
                {
                    DoPerk(character);
                }

                _destroyingAnimation = _sprite.transform.DOScale(.3f, 1f).OnComplete(() =>
                {
                    _destroyingAnimation = null;
                    NetworkObject.Despawn();
                });
            }
        }

        //Randomize a change of giving a new wapon or a new gun
        private void DoPerk(Character character)
        {
            //50% chance of giving ammo and 50% chance of givin a Random Weapon
            float action = Random.Range(0.0f, 1.0f);

            if (action < .5f)
            {
                character.WeaponComponent.CurrentAmmo.Value = character.WeaponComponent.WeaponSO.MaxAmmo;
                MyNetworkManager.ServerLog(new ServerLog($"{ character.DataReference.DisplayName } found ammo !", ServerLog.ServerColor));
            }
            else 
            {
                WeaponManager weaponManager = Game.GetManager<WeaponManager>();
                WeaponSO randomizedWepon = weaponManager.Weapons[Random.Range(0, weaponManager.Weapons.Length)];
                WeaponData weaponData = new WeaponData(randomizedWepon.Id);
                character.WeaponComponent.Initialize(weaponData);
                string weaponDataString = Newtonsoft.Json.JsonConvert.SerializeObject(weaponData);
                Game.GetManager<MyNetworkManager>().SendWeaponDataToClientRpc(character.NetworkObjectId, weaponDataString);
                MyNetworkManager.ServerLog(new ServerLog($"{ character.DataReference.DisplayName } found { randomizedWepon.Id } gun !", ServerLog.ServerColor));
            }

            if (character is Player)
            {
                Game.GetManager<MyNetworkManager>().SendScoreToClientRpc(5, character.DataReference.ClientId);
            }
        }
    }
}