using UnityEngine;

namespace Monomyto.diepio
{
    public class Entity<Data> : MonoBehaviour
        where Data : class
    {
        public Data DataReference => _data;

        public bool IsInitalized => _data != null;

        [SerializeField]
        private Data _data;

        public virtual void Initialize(Data data) 
        {
            _data = data;
        }
    }
}