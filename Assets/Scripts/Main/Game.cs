using UnityEngine;
using System;
using System.Collections.Generic;

namespace Monomyto.diepio
{
    public class Game : SingletonMonobehaviour<Game>
    {
        public enum GameState { None, Intro, Gameplay, Death, Server, Global }
        
        public static GameState CurrentState => instance != null ? instance._currentState : GameState.None;
        public static event Action<GameState> OnStateChange;
        public static Camera GameCamera;

        public static bool IsServerSide => instance != null ? instance.IsServer : false;

        private static Dictionary<string, ManagerObject> RegisteredManagers = new Dictionary<string, ManagerObject>();
        
        [SerializeField]
        private GameState _currentState;
        [SerializeField]
        private GameState _toState;

        protected override void Awake() 
        {
            base.Awake();

            GameCamera = Camera.main;

            //Starting the game in menu
            SetStateInternal(GameState.Intro);

        }

        private void Update()
        {
            UpdateStateChange();

            foreach (ManagerObject managerObject in RegisteredManagers.Values) 
            {
                managerObject.Run();
            }
        }

        private void SetStateInternal(GameState toState) 
        {
            _toState = toState;
        }

        private void UpdateStateChange() 
        {
            if (_toState != _currentState) 
            {
                _currentState = _toState;
                OnStateChange?.Invoke(_currentState);
            }
        }

        #region - Static - 

        public static void SetState(GameState toState) 
        {
            if (instance == null) 
            {
                Debug.LogError("Instance is null !");
                return;
            }

            instance.SetStateInternal(toState);
        }

        public static void RegisterManager(ManagerObject manager)
        {
            RegisteredManagers.Add(manager.GetType().Name, manager);
        }

        public static void UnregisterManager(ManagerObject manager) 
        {
            RegisteredManagers.Remove(manager.GetType().Name);
        }

        public static ManagerObjectType GetManager<ManagerObjectType>()
            where ManagerObjectType : ManagerObject
        {
            if (RegisteredManagers.TryGetValue(typeof(ManagerObjectType).Name, out ManagerObject manager)) 
            {
                return manager as ManagerObjectType;
            }

            Debug.LogError($"There is no manager of type { typeof(ManagerObjectType).Name } registered in the game");
            return null;
        }

        #endregion
    }
}