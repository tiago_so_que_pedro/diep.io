using UnityEngine;

namespace Monomyto.diepio
{
    [System.Serializable]
    public struct BoxData 
    {
        public int Team;
        public int Score;

        public BoxData(int team, int score) 
        {
            Team = team;
            Score = score;
        }
    }
}