# README #

Projeto feito como desafio proposto pela empresa Monomyto.
O jogo consiste em uma inspiração do jogo diep.io.

### Projeto ###

Ao baixar o repositório, você encontrará uma pasta de relase. Dentro dele vai ter uma pasta com o nome do jogo. Lá dentro tem mais duas pastas, sendo uma responsável por
rodar o servidor e a outra pasta responsável por rodar o jogo.

O cliente se conecta com o servidor a partir do momento que usuário define um nome para o seu personagem. Então, saiba que antes de rodar qualquer cliente é necessário que o
servidor esteja previamente aberto.

### Multiplayer ###

Foi utilizado a package NetCode, uma nova solução multiplayer da Unity.
Neste projeto, toda comunicação que acontece entre cliente e servidor é através da classe MyNetworkManager. 
