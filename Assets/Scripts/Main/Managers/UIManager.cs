using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

namespace Monomyto.diepio
{
    public class UIManager : ManagerObject
    {
        [SerializeField]
        private UIPanel[] _registeredPanels;

        public override bool Run()
        {
            if (base.Run())
            {
                return true;
            }

            return false;
        }

        protected override void OnObjectDisabled()
        {
            Game.UnregisterManager(this);
            Game.OnStateChange -= ChangePanelOnStateChange;
        }

        protected override void OnObjectEnabled()
        {
            Game.RegisterManager(this);

            HideAllPanels();
            Game.OnStateChange += ChangePanelOnStateChange;
        }

        private void ChangePanelOnStateChange(Game.GameState state)
        {
            foreach (UIPanel panel in _registeredPanels)
            {
                if (panel.TargetState == state && !NetworkManager.Singleton.IsConnectedClient && panel.WaitLocalClientConnection) 
                {
                    StartCoroutine(DisplayOnNetworkConnected( panel ));
                    continue;
                }

                panel.gameObject.SetActive(panel.TargetState == state);
            }
        }

        IEnumerator DisplayOnNetworkConnected(UIPanel panel) 
        {
            while (Game.GetManager<CharacterManager>().LocalNetworkObjectId == 0) 
            {
                yield return null;
            }

            panel.gameObject.SetActive(true);
        }

        private void HideAllPanels()
        {
            foreach (UIPanel panel in _registeredPanels)
            {
                panel.gameObject.SetActive(false);
            }
        }
    }
}