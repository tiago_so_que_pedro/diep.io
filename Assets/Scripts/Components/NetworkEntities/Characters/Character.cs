using UnityEngine;
using Unity.Netcode;
using System.Linq;

namespace Monomyto.diepio
{
    public class Character : NetworkEntity<CharacterData>
    {

        public SpriteRenderer Sprite => _sprite;

        public NetworkVariable<int> Team = new NetworkVariable<int>();
        public NetworkVariable<Vector2> WalkY = new NetworkVariable<Vector2>();
        public NetworkVariable<Vector2> WalkX = new NetworkVariable<Vector2>();

        public Collider2D Collider => _collider;
        public WeaponComponent WeaponComponent => _weaponComponent;

        [SerializeField]
        protected SpriteRenderer _sprite;
        [SerializeField]
        protected Collider2D _collider;
        [SerializeField]
        protected WeaponComponent _weaponComponent;

        protected Character _localCharacterReference;

        public override void Initialize(CharacterData data)
        {
            base.Initialize(data);
        }

        public override void OnNetworkSpawn()
        {
            base.OnNetworkSpawn();

            _weaponComponent.Initialize(WeaponData.Default);
        }

        public override void OnNetworkDespawn()
        {
            base.OnNetworkDespawn();

            if (NetworkObjectId == Game.GetManager<CharacterManager>().LocalNetworkObjectId) 
            {
                Game.SetState(Game.GameState.Death);
            }
            
            Game.GetManager<CharacterManager>().InGameCharacterByObjectId.Remove(NetworkObjectId);
            Game.GetManager<CharacterManager>().InGameCharacterColliders.Remove(Collider);
        }

        protected virtual void Update()
        {
        }

        protected virtual void Start()
        {
            if (!NetworkManager.Singleton.IsServer)
            {
                Game.GetManager<CharacterManager>().InGameCharacterByObjectId.Add(NetworkObjectId, this);
            }
        }

        private void OnDisable()
        {
            if (!NetworkManager.Singleton.IsServer) 
            {
                Game.GetManager<CharacterManager>().InGameCharacterByObjectId.Remove(NetworkObjectId);
            }
        }

        public virtual void Move(Vector2 direction)
        {
            bool canWalk = !Physics2D.Raycast(transform.position, (WalkX.Value + WalkY.Value), 1f, Game.GetManager<LayerManager>().WallLayer);
            Debug.DrawRay(transform.position, (WalkX.Value + WalkY.Value) * 1f, Color.red);

            if (!canWalk) { return; }

            transform.Translate(direction * DataReference.Stats.MoveSpeed * Time.deltaTime);
        }

        public virtual void Face(Vector3 direction) 
        {
            Sprite.transform.up = direction;
        }
    }
}