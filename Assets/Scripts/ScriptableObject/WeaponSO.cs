using UnityEngine;
using Unity.Netcode;

namespace Monomyto.diepio
{
    [CreateAssetMenu(fileName = "GunDataScriptableObject", menuName = "Diep.io/GunData")]
    public class WeaponSO : ScriptableObject
    {
        public string Id => _id;
        public float FireRate => _fireDelay;
        public float Damage => _damage;
        public int MaxAmmo => _maxAmmo;
        public bool BurstFromAllBarrels => _burstFromAllBarrels;
        public WeaponGraphics WeaponGraphics => _weaponGraphics;

        [SerializeField]
        private string _id;
        [SerializeField, Range(0f, 1f)]
        private float _fireDelay;
        [SerializeField, Range(0f, 1f)]
        private float _damage;
        [SerializeField]
        private int _maxAmmo;
        [SerializeField]
        private bool _burstFromAllBarrels;
        [SerializeField]
        private WeaponGraphics _weaponGraphics;
    }
}