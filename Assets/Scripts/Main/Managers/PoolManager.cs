using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

namespace Monomyto.diepio
{
    public class PoolManager : ManagerObject
    {

        public Dictionary<NetworkObject, BulletComponent> BulletsPerNetworkObject => _bulletsPerNetworkObject;
        public Dictionary<NetworkObject, Box> BoxPerNetworkObject => _boxesPerNetworkObject;

        [SerializeField]
        private PoolCofig[] _poolConfigs;

        private Dictionary<GameObject, Queue<NetworkObject>> _pools = new Dictionary<GameObject, Queue<NetworkObject>>();
        private Dictionary<NetworkObject, BulletComponent> _bulletsPerNetworkObject = new Dictionary<NetworkObject, BulletComponent>();
        private Dictionary<NetworkObject, Box> _boxesPerNetworkObject = new Dictionary<NetworkObject, Box>();

        public NetworkObject GetInstanceFromPool(NetworkObject prefab, Vector3 position, Quaternion rotation) 
        {
            Queue<NetworkObject> desiredPool = _pools[prefab.gameObject];

            NetworkObject pooledInstance;

            if (desiredPool.Count > 0)
            {
                pooledInstance = desiredPool.Dequeue();
            }
            else 
            {
                pooledInstance = Instantiate(prefab);
            }

            pooledInstance.gameObject.SetActive(true);
            pooledInstance.transform.position = position;
            pooledInstance.transform.rotation = rotation;

            return pooledInstance;
        }

        private void InitializePools() 
        {
            foreach (PoolCofig poolCofig in _poolConfigs) 
            {
                Queue<NetworkObject> queue = new Queue<NetworkObject>();
                _pools.Add(poolCofig.Prefab.gameObject, queue);

                for (int i = 0; i < poolCofig.Count; i++) 
                {
                    NetworkObject instace = Instantiate(poolCofig.Prefab);
                    ReturnToPool(instace, poolCofig.Prefab.gameObject);
                }

                NetworkManager.Singleton.PrefabHandler.AddHandler(poolCofig.Prefab, new PooledPrefabInstanceHandler(poolCofig.Prefab, this));
                NetworkManager.Singleton.PrefabHandler.AddHandler(poolCofig.Prefab.gameObject, new PooledPrefabInstanceHandler(poolCofig.Prefab, this));
            }
        }

        public void ReturnToPool(NetworkObject pooledObject, GameObject prefab) 
        {
            pooledObject.gameObject.SetActive(false);
            _pools[prefab].Enqueue(pooledObject);
        }

        public NetworkObject GetPrefab(int poolId) 
        {
            return _poolConfigs[poolId].Prefab;
        }

        protected override void OnObjectDisabled()
        {
            Game.UnregisterManager(this);
        }

        protected override void OnObjectEnabled()
        {
            Game.RegisterManager(this);
        }

        public override void OnNetworkSpawn()
        {
            if (_poolConfigs.Length != _pools.Count)
            {
                InitializePools();
            }
        }
    }

    [System.Serializable]
    struct PoolCofig 
    {
        public NetworkObject Prefab;
        public int Count;
    }

    class PooledPrefabInstanceHandler : INetworkPrefabInstanceHandler
    {

        NetworkObject _prefab;
        PoolManager _poolManager;

        public PooledPrefabInstanceHandler(NetworkObject prefab, PoolManager poolManager) 
        {
            _prefab = prefab;
            _poolManager = poolManager;
        }

        public NetworkObject Instantiate(ulong ownerClientId, Vector3 position, Quaternion rotation)
        {
            return _poolManager.GetInstanceFromPool(_prefab, position, rotation);
        }

        public void Destroy(NetworkObject networkObject)
        {
            _poolManager.ReturnToPool(networkObject, _prefab.gameObject);
        }

    }
}