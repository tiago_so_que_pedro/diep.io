using UnityEngine;

namespace Monomyto.diepio
{
    public class SingletonMonobehaviour<T> : Unity.Netcode.NetworkBehaviour
        where T : MonoBehaviour
    {

        protected static T instance;

        protected virtual void Awake()
        {

            if (instance == null)
            {

                instance = this as T;
                DontDestroyOnLoad(this.gameObject);
            }
            else
            {
                Destroy(this);
            }
        }
    }
}