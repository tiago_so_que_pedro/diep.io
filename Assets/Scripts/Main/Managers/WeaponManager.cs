using UnityEngine;
using Unity.Netcode;
using System.Collections.Generic;

namespace Monomyto.diepio
{
    public class WeaponManager : ManagerObject
    {

        public WeaponSO[] Weapons => _weaponsSO;

        [SerializeField]
        private WeaponSO[] _weaponsSO;
        
        [SerializeField]
        private int _prefabId;

        //Server-side
        private Dictionary<BulletComponent, Character> _bulletsOwner;

        protected override void OnObjectDisabled()
        {
            Game.UnregisterManager(this);
        }

        protected override void OnObjectEnabled()
        {
            Game.RegisterManager(this);
        }

        public void Shoot(NetworkClient client) 
        {
            Character character = Game.GetManager<CharacterManager>().InGamePlayers[client];
            Shoot(character);
        }

        public void Shoot(Character character) 
        {
            WeaponComponent weapon = character.WeaponComponent;

            if (weapon.CurrentAmmo.Value <= 0)
            {
                return;
            }

            PoolManager poolManager = Game.GetManager<PoolManager>();

            if (!weapon.WeaponSO.BurstFromAllBarrels)
            {
                Transform barrel = weapon.WeaponGraphics.GetRandomBarrel();
                NetworkObject instantiatedBullet = poolManager.GetInstanceFromPool(poolManager.GetPrefab(_prefabId), barrel.position, Quaternion.identity);
                InitializeInstantiatedBullet(instantiatedBullet, character.DataReference.ClientId, character, barrel);
            }
            else
            {
                for (int i = 0; i < weapon.WeaponGraphics.Barrels.Length; i++)
                {
                    Transform barrel = weapon.WeaponGraphics.Barrels[i];
                    NetworkObject instantiatedBullet = poolManager.GetInstanceFromPool(poolManager.GetPrefab(_prefabId), barrel.position, Quaternion.identity);
                    InitializeInstantiatedBullet(instantiatedBullet, character.DataReference.ClientId, character, barrel);
                }
            }

            weapon.CurrentAmmo.Value--;
        }

        public WeaponSO GetWeaponSOById(string id) 
        {
            for (int i = 0; i < _weaponsSO.Length; i++) 
            {
                if (_weaponsSO[i].Id == id) 
                {
                    return _weaponsSO[i];
                }
            }

            Debug.LogError($"Could not find WeaponSO with id { id }. Are you sure there is a gun with that Id ?");

            return null;
        }

        private void InitializeInstantiatedBullet(NetworkObject bulletNetworkObject, ulong clientId, Character character, Transform barrel)
        {
            PoolManager poolManager = Game.GetManager<PoolManager>();
            BulletComponent bullet;
            if (!poolManager.BulletsPerNetworkObject.TryGetValue(bulletNetworkObject, out bullet)) 
            {
                bullet = bulletNetworkObject.GetComponent<BulletComponent>();
                poolManager.BulletsPerNetworkObject.Add(bulletNetworkObject, bullet);
            }

            bullet.ClientId.Value = clientId;
            bullet.ShooterObjectId.Value = character.NetworkObjectId;
            bullet.Initialize(new BulletData(character, barrel.transform.position, bulletNetworkObject.transform.position + barrel.transform.up * 10f, 2f));
            bullet.NetworkObjectReference.SpawnWithOwnership(OwnerClientId);
        }
    }
}