using UnityEngine;
using System.Collections.Generic;
using Unity.Netcode;

namespace Monomyto.diepio
{
    public class CharacterManager : ManagerObject
    {

        public const int MaxTeams = 1;

        public static bool UpdateCharacters { get; private set; }

        //Server only
        public Dictionary<NetworkClient, Character> InGamePlayers => _inGamePlayers;
        public Dictionary<Collider2D, Character> InGameCharacterColliders => _inGameCharacterColliders;

        //Client only
        public Dictionary<ulong, Character> InGameCharacterByObjectId => _inGameCharacterByObjectId;

        public ulong LocalNetworkObjectId;

        [SerializeField]
        private Player _playerPrefab;
        [SerializeField]
        private Enemy _enemyPrefab;

        private Dictionary<NetworkClient, Character> _inGamePlayers;
        private Dictionary<Collider2D, Character> _inGameCharacterColliders;
        private Dictionary<ulong, Character> _inGameCharacterByObjectId;

        private int _serverCurrentTeam = 0;

        protected override void OnObjectDisabled()
        {
            Game.UnregisterManager(this);
            UpdateCharacters = false;
        }

        protected override void OnObjectEnabled()
        {
            Game.RegisterManager(this);
            UpdateCharacters = true;
            
            _inGamePlayers = new Dictionary<NetworkClient, Character>();
            _inGameCharacterColliders = new Dictionary<Collider2D, Character>();

            if (!IsServer)
            {
                _inGameCharacterByObjectId = new Dictionary<ulong, Character>();
            }
        }

        public override bool Run()
        {
            if (base.Run())
            {
                UpdateCharacters = true;
                return true;
            }

            UpdateCharacters = false;

            return false;
        }

        public void ServerCreatePlayer(CharacterData characterData, NetworkClient client)
        {
            Player instantiatedPlayer = Instantiate(_playerPrefab, Vector3.zero, Quaternion.identity);
            instantiatedPlayer.NetworkObject.SpawnWithOwnership(OwnerClientId);

            _serverCurrentTeam++;
            instantiatedPlayer.Team.Value = Mathf.RoundToInt(Mathf.PingPong(_serverCurrentTeam, MaxTeams));

            MyNetworkManager.ServerLog(new ServerLog($"Set { characterData.DisplayName } on team { instantiatedPlayer.Team.Value }", ServerLog.ServerColor));

            instantiatedPlayer.Initialize(characterData);

            InGamePlayers.Add(client, instantiatedPlayer);
            InGameCharacterColliders.Add(instantiatedPlayer.Collider, instantiatedPlayer);
        }

        public void ServerRemovePlayer(NetworkClient client) 
        {
            if (!InGamePlayers.ContainsKey(client))
            {
                return;
            }

            Character player = InGamePlayers[client];
            
            InGamePlayers.Remove(client);
            ServerRemoveCollider(player);    
            player.NetworkObject.Despawn();
        }

        public void ServerRemoveCollider(Character character) 
        {
            InGameCharacterColliders.Remove(character.Collider);
        }

        public void ServerRotatePlayer(NetworkClient client, Vector3 direction) 
        {
            if (!InGamePlayers.ContainsKey(client)) 
            {
                return;
            }

            Character character = InGamePlayers[client];
            character.Face(direction);
        }

        public void ServerMovePlayer(NetworkClient client, Vector2 direction) 
        {
            if (!InGamePlayers.ContainsKey(client)) 
            {
                return;
            }

            Character character = InGamePlayers[client];

            if (direction == Vector2.up || direction == -Vector2.up)
            {
                if (character.WalkY.Value == Vector2.zero)
                {
                    MyNetworkManager.ServerLog(new ServerLog($"{ character.DataReference.DisplayName } requested for move { direction }", ServerLog.ServerColor));
                }

                character.WalkY.Value = direction;
            }
            else if (direction == Vector2.right || direction == -Vector2.right)
            {
                if (character.WalkX.Value == Vector2.zero)
                {
                    MyNetworkManager.ServerLog(new ServerLog($"{ character.DataReference.DisplayName } requested for move { direction }", ServerLog.ServerColor));
                }

                character.WalkX.Value = direction;
            }
        }

        public void ServerStopMovePlayer(NetworkClient client, Vector2 direction) 
        {
            if (!InGamePlayers.ContainsKey(client)) 
            {
                return;
            }

            Character character = InGamePlayers[client];
            if (direction == Vector2.up)
            {
                character.WalkY.Value = Vector2.zero;
            }
            else if (direction == Vector2.right) 
            {
                character.WalkX.Value = Vector2.zero;
            } 

            MyNetworkManager.ServerLog(new ServerLog($"{ character.DataReference.DisplayName} requested for stop move", ServerLog.ServerColor));
        }

        public void ServerKillPlayer(ulong clientId) 
        {
            NetworkClient networkClient = NetworkManager.Singleton.ConnectedClients[clientId];

            if (!InGamePlayers.ContainsKey(networkClient))
            {
                return;
            }

            ServerRemovePlayer(networkClient);
        }

        public void SpawnRandomEnemies() 
        {
            for (int i = 0; i < 10; i++) 
            {
                Vector3 position = new Vector3(Random.Range(-26f, 26), Random.Range(-19f, 19f), 0);
                Enemy spawnedEnemy = Instantiate(_enemyPrefab, position, Quaternion.identity);
                spawnedEnemy.Team.Value = 2;

                spawnedEnemy.Initialize(CharacterData.DefaultEnemyCharacterData);
                spawnedEnemy.NetworkObject.SpawnWithOwnership(OwnerClientId);
                
                InGameCharacterColliders.Add(spawnedEnemy.Collider, spawnedEnemy);
            }
        }
    }
}