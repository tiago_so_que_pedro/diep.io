using System;
using Unity.Netcode;

[Serializable]
public class CharacterData
{

    public static CharacterData DefaultEnemyCharacterData => new CharacterData(CharacterStats.DefaultEnemy, 0, "Enemy");

    public ulong ClientId => _clientId;
    public string DisplayName => _displayName;
    public CharacterStats Stats => _stats;

    [UnityEngine.SerializeField]
    private ulong _clientId;
    [UnityEngine.SerializeField]
    private string _displayName;
    [UnityEngine.SerializeField]
    private CharacterStats _stats;

    public CharacterData(CharacterStats stats, ulong clientId, string displayName) 
    {
        _clientId = clientId;
        _displayName = displayName;
        _stats = stats;
    }
}

[Serializable]
public class CharacterStats 
{

    public static CharacterStats Default => new CharacterStats(life: 1, moveSpeed: 2, rotationSpeed: 5);
    public static CharacterStats DefaultEnemy => new CharacterStats(life: .05f, moveSpeed: 1.5f, rotationSpeed: 5);

    public float Life { get => _life; set => _life = value; }
    public float MoveSpeed => _moveSpeed;
    public float RotationSpeed => _rotationSpeed;

    [UnityEngine.SerializeField]
    private float _life;
    [UnityEngine.SerializeField]
    private float _moveSpeed;
    [UnityEngine.SerializeField]
    private float _rotationSpeed;

    public CharacterStats(float life, float moveSpeed, float rotationSpeed)
    {
        _life = life;
        _moveSpeed = moveSpeed;
        _rotationSpeed = rotationSpeed;
    }
}
