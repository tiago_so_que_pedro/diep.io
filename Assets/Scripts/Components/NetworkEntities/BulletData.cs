using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Monomyto.diepio
{
    [System.Serializable]
    public class BulletData 
    {
        public Character Shooter => _shooter;
        public Vector2 StartPosition => _startPosition;
        public Vector2 Destination => _destination;
        public float TravelDuration => _travelDuration;
        
        [SerializeField]
        private Character _shooter;
        [SerializeField]
        private Vector2 _startPosition;
        [SerializeField]
        private Vector2 _destination;
        [SerializeField]
        private float _travelDuration;

        public BulletData(Character shooter, Vector2 startPosition, Vector2 destination, float travelDuration) 
        {
            _shooter = shooter;

            _startPosition = startPosition;

            Vector2 forward = (destination - startPosition).normalized;
            Vector2 right = new Vector2(forward.y, -forward.x);

            //Destination will be the absolute destination plus a random offset given on right and forward
            _destination = destination + (forward * Random.Range(-2f, 2f)) + (right * Random.Range(-2f, 2f));
            _travelDuration = travelDuration;
        }
    }
}