using UnityEngine;
using Unity.Netcode;

namespace Monomyto.diepio
{
    public abstract class ManagerObject : NetworkBehaviour
    {

        public Game.GameState TargetState => _targetState;

        [SerializeField]
        private Game.GameState _targetState;

        protected abstract void OnObjectEnabled();

        protected abstract void OnObjectDisabled();

        private void OnEnable()
        {
            Initalize();
            OnObjectEnabled();
        }

        private void OnDisable()
        {
            OnObjectDisabled();
        }

        public virtual void Initalize()
        {
        }

        public virtual bool Run() 
        {
            return _targetState == Game.GameState.Global || _targetState == Game.CurrentState;
        }
    }
}