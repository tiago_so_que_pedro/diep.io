using UnityEngine;
using TMPro;
using Unity.Netcode;

namespace Monomyto.diepio
{
    public class NetworkedTextMeshProUGUI : NetworkEntity<string>
    {
        [SerializeField]
        private TextMeshProUGUI _textLabel;

        public override void Initialize(string data)
        {
            base.Initialize(data);
            _textLabel.text = data;
        }

        public override void OnNetworkObjectParentChanged(NetworkObject parentNetworkObject)
        {
            transform.SetParent(parentNetworkObject.transform);
        }
    }
}