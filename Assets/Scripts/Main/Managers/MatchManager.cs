using UnityEngine;
using Unity.Netcode;
using System;

namespace Monomyto.diepio
{
    public class MatchManager : ManagerObject
    {
        public int Score 
        {
            get => _score;
            set => _score = value;
        }

        [SerializeField]
        private int _score;

        protected override void OnObjectDisabled()
        {
            Game.UnregisterManager(this);
        }

        protected override void OnObjectEnabled()
        {
            Game.RegisterManager(this);
        }

        public void StartMatch(string displayName) 
        {
            _score = 0;
        }
    }
}