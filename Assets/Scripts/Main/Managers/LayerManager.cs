using UnityEngine;

namespace Monomyto.diepio
{
    public class LayerManager : ManagerObject
    {

        public LayerMask WallLayer => _wallLayer;
        public LayerMask EnemyLayer => _enemyLayer;
        public LayerMask FriendLayer => _friendLayer;

        [SerializeField]
        private LayerMask _wallLayer;
        [SerializeField]
        private LayerMask _enemyLayer;
        [SerializeField]
        private LayerMask _friendLayer;

        protected override void OnObjectDisabled()
        {
            Game.UnregisterManager(this);
        }

        protected override void OnObjectEnabled()
        {
            Game.RegisterManager(this);
        }
    }
}