using UnityEngine;

namespace Monomyto.diepio
{
    public class WeaponGraphics : MonoBehaviour
    {
        public Transform[] Barrels => _barrels;

        [SerializeField]
        private Transform[] _barrels;

        public Transform GetRandomBarrel() 
        {
            return _barrels[Random.Range(0, _barrels.Length)];
        }
    }
}