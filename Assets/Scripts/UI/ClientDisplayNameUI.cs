using Unity.Netcode;
using UnityEngine;
using TMPro;

namespace Monomyto.diepio
{
    public class ClientDisplayNameUI : Entity<Character>
    {
        [SerializeField]
        private TextMeshProUGUI _nameLbl;

        public override void Initialize(Character data)
        {
            base.Initialize(data);
        }

        private void Start()
        {
        }

        private void OnDisable()
        {
            if (NetworkManager.Singleton == null)
            {
                return;
            }
        }  

        private void Update()
        {
            if (DataReference == null)
            {
                Destroy(gameObject);
                return;
            }

            transform.position = (Vector2)DataReference.transform.position + (Vector2.up * 1.25f);
            _nameLbl.text = DataReference.DataReference.DisplayName;
        }
    }
}