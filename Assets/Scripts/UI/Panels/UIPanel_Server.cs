using System.Collections;
using UnityEngine;
using DG.Tweening;
using TMPro;
using Unity.Netcode;
using System.Collections.Generic;
using UnityEngine.UI;
using Unity.Netcode;

namespace Monomyto.diepio
{
    public class UIPanel_Server : UIPanel
    {

        public const int MaxLog = 50;
        public static List<ServerLog> ServerLog = new List<ServerLog>();

        public static System.Action OnLog;

        [SerializeField]
        private CanvasGroup _usersCountCanvasGroup;
        [SerializeField]
        public TextMeshProUGUI _userCountLbl;
        [Space]
        [SerializeField]
        private CanvasGroup _usersIdCountCanvasGroup;
        [SerializeField]
        public TextMeshProUGUI _userIdCountLbl;
        [Space]
        [SerializeField]
        private CanvasGroup _logContainerCanvasGroup;
        [SerializeField]
        private TextMeshProUGUI _logLabel;
        [Space]
        [SerializeField]
        private CanvasGroup _clientActionsCanvasGroup;
        [SerializeField]
        private Transform _clientActionHolder;

        [SerializeField]
        private ScrollRect _scrollbar;

        [SerializeField]
        private ClientActionUI _clientActionUIPrefab;

        public override void Open()
        {
            StartCoroutine(IEOpen());
            OnLog += OnLogAction;
        }

        private void Start()
        {
            NetworkManager.Singleton.OnClientConnectedCallback += OnClientConnected;
        }

        private void OnDisable()
        {
            if (NetworkManager.Singleton == null) { return; }
            NetworkManager.Singleton.OnClientConnectedCallback -= OnClientConnected;
        }

        IEnumerator IEOpen() 
        {
            _usersCountCanvasGroup.alpha = 0;
            _usersIdCountCanvasGroup.alpha = 0;
            _logContainerCanvasGroup.alpha = 0;
            _clientActionsCanvasGroup.alpha = 0;
            yield return new WaitForEndOfFrame();
            float delay = 0;

            _usersCountCanvasGroup.DOFade(1, .5f).SetDelay(delay);
            delay += .25f;

            _usersIdCountCanvasGroup.DOFade(1, .5F).SetDelay(delay);
            delay += .25f;

            _logContainerCanvasGroup.DOFade(1, .5f).SetDelay(delay);
            delay += .25f;

            _clientActionsCanvasGroup.DOFade(1, .5f).SetDelay(delay);
            delay += .25f;
        }

        private void Update()
        {
            _userCountLbl.text = "Clients: " + NetworkManager.Singleton.ConnectedClients.Count;
            _userIdCountLbl.text = "ClientsIds: " + string.Join(',', NetworkManager.Singleton.ConnectedClientsIds);
        }

        void OnClientConnected(ulong clientId) 
        {
            ClientActionUI clientAction = Instantiate(_clientActionUIPrefab, _clientActionHolder);
            NetworkClient networkClient = NetworkManager.Singleton.ConnectedClients[clientId];
            clientAction.Initialize(networkClient);
        }

        void OnLogAction() 
        {
            string finalLogString = string.Empty;

            foreach (ServerLog log in ServerLog)
            {
                finalLogString += $"<color={log.Color}>{ log.Message }</color>\n";
            }

            _logLabel.text = finalLogString;
            _scrollbar.normalizedPosition = new Vector2(0, 0);

            if (ServerLog.Count > MaxLog) 
            {
                int diff = Mathf.Abs(MaxLog - ServerLog.Count);

                for (int i = 0; i < diff; i++) 
                {
                    ServerLog.RemoveAt(0);
                }
            }
        }
    }
}