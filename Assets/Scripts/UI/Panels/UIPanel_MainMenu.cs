using UnityEngine;
using TMPro;
using System.Collections;
using DG.Tweening;

namespace Monomyto.diepio
{
    public class UIPanel_MainMenu : UIPanel
    {
        [SerializeField]
        private TextMeshProUGUI _title;
        [SerializeField]
        private TMP_InputField _name;

        public override void Open()
        {
            StartCoroutine(IEOpen());
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                Game.SetState(Game.GameState.Gameplay);
                Game.GetManager<MyNetworkManager>().Join(_name.text);
                Game.GetManager<MatchManager>().StartMatch(_name.text);
            }
        }

        private IEnumerator IEOpen() 
        {
            _title.transform.position = Vector3.zero + Vector3.up * 50;
            _name.transform.position = Vector3.zero + Vector3.up * 50;

            yield return new WaitForEndOfFrame();

            _title.rectTransform.DOAnchorPos(Vector3.up * 125, 2f).SetDelay(.1f);
            (_name.transform as RectTransform).DOAnchorPos(Vector3.zero, 2f);
        }
    }
}